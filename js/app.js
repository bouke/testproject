import $ from "jquery";

$(document).ready(function() {

    $('a[href*=\\#]').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop : $(this.hash).offset().top
        }, 800);
    });

    let goUp = $('#go-up');

    window.onscroll = function() {
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            goUp.fadeIn();
        } else {
            goUp.fadeOut();
        }
    }

});
